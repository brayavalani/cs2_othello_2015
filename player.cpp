#include "player.h"
#include <algorithm> //for min and max


/*
 * Constructor for the player; initialize everything here. 
 * The side your AI is
 * on (BLACK or WHITE) is passed in as "side". The constructor must finish 
 * within 30 seconds.
 */
Player::Player(Side side) {
    // Will be set to true in test_minimax.cpp.
    testingMinimax = false;
    minMaxDepth = 4;

    /* 
     * TODO: Do any initialization you need to do here (setting up the board,
     * precalculating things, etc.) However, remember that you will only have
     * 30 seconds.
     */

    mySide = side;
    oppSide = (side == BLACK) ? WHITE : BLACK;
    
}

/*
 * Destructor for the player.
 */
Player::~Player() {
}

/*
 * Compute the next move given the opponent's last move. Your AI is
 * expected to keep track of the board on its own. If this is the first move,
 * or if the opponent passed on the last move, then opponentsMove will be NULL.
 *
 * msLeft represents the time your AI has left for the total game, in
 * milliseconds. doMove() must take no longer than msLeft, or your AI will
 * be disqualified! An msLeft value of -1 indicates no time limit.
 *
 * The move returned must be legal; if there are no valid moves for your side,
 * return NULL.
 */
Move *Player::doMove(Move *opponentsMove, int msLeft) {
    /* 
     * TODO: Implement how moves your AI should play here. You should first
     * process the opponent's opponents move before calculating your own move
     */
    if (opponentsMove != NULL)
        myBoard.doMove(opponentsMove,oppSide);

    //first step: working AI
    //return a random legal move for now
    //Move* move = randomValidMove();

    //second step: heuristic AI
    //return a move ordered via heuristic
    
    //thirdStep: minimax AI
    //return a move ordered via minimax tree
    return MinMaxMove();
    
}

/* internal random legal move function */
Move *Player::randomValidMove() {
    Move* randomMove = NULL;
    for (int i = 0; i < 8; i++) {
        for (int j = 0; j < 8; j++) {
            randomMove = new Move(i, j);
            if (myBoard.checkMove(randomMove, mySide))
            {
                //myBoard.doMove(randomMove,mySide);
                return randomMove;
            }
            else
            {
                delete randomMove;
            }
        }
    }
    return NULL;
}

/* internal heuristic based legal move function */
Move *Player::heuristicValidMove() {
    
    Move* returnMove = NULL;
    Move* randomMove = NULL;
    
    int moveValue = -100, currentMoveValue;
    Board* temp = NULL;
    for (int i = 0; i < 8; i++) {
        for (int j = 0; j < 8; j++) {
            randomMove = new Move(i, j);
            if (myBoard.checkMove(randomMove, mySide))
            {
                
                    temp = myBoard.copy();
                    temp->doMove(randomMove,oppSide);
                    currentMoveValue = simpleHeuristic(temp, randomMove);
                    //std::cerr << currentMoveValue <<",";
    
                    delete temp;
                
                //select by greater heuristic value
                if( returnMove == NULL || currentMoveValue > moveValue)
                {
                    moveValue = currentMoveValue;
                    //delete returnMove;
                    returnMove = new Move(i, j);
                }
                   
            }
             delete randomMove;
            
        }
    }
    //myBoard.doMove(returnMove,mySide);
    return returnMove;
}



/* internal function to calculate naive heuristic */
int Player::naiveHeuristic(Board* temp,Move* move) {
    
    //calculate heuristic
    double moveValue = temp->count(mySide) - temp->count(oppSide);
              
    //return heuristic
    return (int)moveValue;
}

//set board for testminimax program
void Player::setBoard(char data[]) {
    myBoard.setBoard(data);
}

/* internal function ofr minmax move */
Move *Player::MinMaxMove() {
    //starting alpha beta values
    double alpha = -223.0;
	double beta = 223.0;
	
	std::vector<Node*>* c = new std::vector<Node*>();
	std::vector<Move*>* m = possibleMoves(&myBoard,mySide); //get possible moves
	Node* parentNode = NULL;
	
	if (m->empty())
	{
		return NULL;
	}
	
	//converts from m to treeNode	
	for (std::vector<Move*>::iterator i = m->begin(); i != m->end(); i++)
	{
		Node* nextNode = new Node(parentNode,*i);
		c->push_back(nextNode);	
		Board* boardCopy = myBoard.copy();
		boardCopy->doMove(*i,mySide);
		minMaxMove(minMaxDepth-1,boardCopy,oppSide,nextNode,alpha,beta); //depth,board for move,side,parent
		
	}
	
	Move* returnMove = (findMax(c))->getCurr();
	myBoard.doMove(returnMove,mySide);
	
	//free memory
    delete m; 
    delete c; 
	
    return returnMove;
}

//Recursive internal Minimax algorithm
double Player::minMaxMove(int depth,Board* board, Side side, Node* node, double alpha, double beta)
{
	Move* move = node->getCurr(); //get move from node
	
	if (depth == 0)	//base case
	{
		double score = simpleHeuristic(board,move); 
		move->setScore(score);
		return score;
	}
	else
	{
		std::vector<Move*>* m = possibleMoves(board,side);	//get possible moves
		
		if (m->empty()) 	
		{
			double score = simpleHeuristic(board,move); 
			move->setScore(score);
			delete m;
			return score;
		}
		else
		{
			std::vector<Node*>* c = new std::vector<Node*>();
			//setup node children
			if (side == mySide)
			{
				for (std::vector<Move*>::iterator i = m->begin(); i != m->end(); i++)
				{
					Node* nextNode = new Node(node,*i);
					c->push_back(nextNode);	
					
					Board* boardCopy = board->copy();
					boardCopy->doMove(*i,side);
                    //get new alpha
					alpha = max(alpha,minMaxMove(depth-1,boardCopy,flip(side),nextNode,alpha,beta)); 
					
					
					if (beta <= alpha) //prune tree
					{
						break;
					}
				}
				
				node->setC(c);	//if use later
				move->setScore(alpha);
				delete m;
				return alpha;
			}
			else  		//for beta
			{
				for (std::vector<Move*>::iterator i = m->begin(); i != m->end(); i++)
				{
					Node* nextNode = new Node(node,*i);
					c->push_back(nextNode);	
					
					Board* boardCopy = board->copy();
					boardCopy->doMove(*i,side);
                    //update beta
					beta = min(beta,minMaxMove(depth-1,boardCopy,flip(side),nextNode,alpha,beta)); 
					
					
					if (beta <= alpha) //prune
					{
						break;
					}
				}
				
				node->setC(c);
				move->setScore(beta);
				delete m; //free
				return beta;
			}

            
		}
	}
}


//Returns of list of all possile moves by the side given as parameter
std::vector<Move*>* Player::possibleMoves(Board* tempBoard,Side side)
{
    //possibleMoves list pM
    std::vector<Move*>* pM = new std::vector<Move*>();
    Move* it = new Move(0,0);
    for (int i = 0; i < 8; i++)
    {
        for (int j = 0; j < 8; j++)
        {
            it->setX(i);
            it->setY(j);
            if (tempBoard->checkMove(it, side))
            {
                Move* possible = new Move(i, j);
                pM->push_back(possible);
            }
        }
    }
    
    delete it; //free
    return pM;
}

/* internal function to calculate heuristic */
double Player::simpleHeuristic(Board* temp,Move* move)  {
    
    
    //calculate heuristic
    double moveValue = temp->count(mySide) - temp->count(oppSide);
    
    //modify heuristic to make it better
    //give priority if corner play or edge play
    int i = move->getX(), j = move->getY();
    if( i==0 || i==7) //row edge
    {
        if(j==0 || j==7) //corner
            moveValue *= 3;
        else if( j==1 || j==6) //adjacent to corner
            moveValue *= -3;
        else //on edge
            moveValue *= 1.5;           
    }
    else if( j==0 || j==7) // on column edge
    {
        //already handled corners
        //handle adjacent
        if( i==1 || i==6) //adjacent to corner
            moveValue *= -3;
        else //on edge
            moveValue *= 1.5;
    }
    
            
    //return heuristic
    return moveValue;
}


//get oppposite side
Side Player::flip(Side side)
{
	return (side == BLACK) ? WHITE : BLACK;	
}
//Finds the min heuristic score associated with a move contained within
// a Node element in the list passed as a parameter
Node* Player::findMax (std::vector<Node*>* list)
{
    Node* m = list->front();
    for(std::vector<Node*>::iterator i = list->begin(); i != list->end(); i++)
    {
        if( ((*i)->getCurr()->getScore()) > (m->getCurr()->getScore()) )
        {
            m = *i;
        }
    }
    return m;
}
