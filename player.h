#ifndef __PLAYER_H__
#define __PLAYER_H__

#include <iostream>
#include "common.h"
#include "board.h"
#include <vector>
using namespace std;

struct Node
{
  Node* p;
  vector<Node*>* c;
  Move* curr;
  
  Node(Node* parentNode, Move* move, vector<Node*>* childList)
  {
    p = parentNode;
    curr = move;
    c = childList;
  }

  Node(Node* parentNode, Move* move)
  {
    p = parentNode;
    curr = move;
  }

  Node* getP()
  {
    return p;
  }

  void setP(Node* parentNode)
  {
    p = parentNode;
  }

  vector<Node*>* getC()
  {
    return c;
  }

  void setC(vector<Node*>* childList)
  {
    c = childList;
  }

  Move* getCurr()
  {
    return curr;
  }

};

class Player {
  
 public:
  Player(Side side);
  ~Player();
  
  Move *doMove(Move *opponentsMove, int msLeft);
  
  // Flag to tell if the player is running within the test_minimax context
  bool testingMinimax;
  void setBoard(char data[]);
  
 private:
  int minMaxDepth;

  Side mySide, oppSide;

  Board myBoard;

  Move *randomValidMove();

  Move *heuristicValidMove();

  Move *MinMaxMove();

  double simpleHeuristic(Board* temp,Move* move);

  int naiveHeuristic(Board* temp,Move* move);
  
  Side flip(Side side);

  vector<Move*>* possibleMoves(Board* tempBoard,Side side);

  double minMaxMove(int depth,Board* board, Side side, Node* node, double alpha, double beta);

  Node* findMax (vector<Node*>* list);
  
};

#endif
